var express = require("express");
var port = process.env.PORT || 3000;
var app = express();
var bodyParser = require('body-parser');
const request = require('request')

app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());

var comments = []
app.get("/", function (req, res) {
 res.send(JSON.stringify(comments));
});
app.post("/comment", function (req, res) {
    res.sendStatus(200)
    let name = req.body.pullrequest.title
    let comment = req.body.comment.content.raw
    let link = req.body.comment.links.html.href
    let actor = req.body.actor.display_name
    var options
    if (comment.includes('pr-change')) {
        options = {
            uri: "https://hooks.slack.com/services/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",
            method: 'POST',
            json: {
                "attachments": [
                    {
                        "color": "#bb0000",
                        "title": actor + " has requested changes on " + name + " 🚫",
                        "title_link": link,
                        "text": comment.replace('pr-change',''),
                        "image_url": "http://my-website.com/path/to/image.jpg",
                        "thumb_url": "http://example.com/path/to/thumb.png",
                        "footer": "Slack API",
                        "footer_icon": "https://platform.slack-edge.com/img/default_application_icon.png",
                        "ts": 123456789
                    }
                ]
            }
          };
    } else if (comment.includes('pr-done')) {
        options = {
            uri: "https://hooks.slack.com/services/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",
            method: 'POST',
            json: {
                "attachments": [
                    {
                        "color": "#4BB543",
                        "title": actor + " has fixed feedback on " + name + " ✅",
                        "title_link": link,
                        "text": comment.replace('pr-done',''),
                        "footer": "Slack API",
                        "footer_icon": "https://platform.slack-edge.com/img/default_application_icon.png",
                        "ts": 123456789
                    }
                ]
            }
          };
    }
    // comments.push({"comment": req.body.comment.content.raw, "actor": req.body.actor.display_name, "repository": req.body.repository.name})
    request.post(options, (error, body, response) => {
        if (error) {
            console.error(error)
            return
        }
        console.log(`statusCode: ${res.statusCode}`)
        console.log(body)
    })
})
app.listen(port, function () {
 console.log(`Example app listening on port !`);
});
